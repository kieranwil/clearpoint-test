import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from 'Store';
import './index.css';
import OurPeople from 'Containers/OurCompany';

ReactDOM.render(
  <Provider store={configureStore()}>
    <OurPeople />
  </Provider>,
  document.getElementById('root')
);
