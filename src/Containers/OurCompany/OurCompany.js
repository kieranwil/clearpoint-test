import React from 'react';
import Loading from 'Components/Loading';
import Container from 'Components/Container';
import Header from 'Components/Header';
import People from 'Components/People/People';
import style from './style.module.scss';

class OurCompany extends React.PureComponent {
  componentDidMount() {
    this.props.getCompanyData();
  }

  header() {
    const { companyName, companyMotto, companyEst } = this.props.companyData.companyInfo;
    const prettyTime = new Date(companyEst).toLocaleDateString();
    return (
      <Header
        primary={companyName}
        secondaryLeft={companyMotto}
        secondaryRight={`Since ${prettyTime}`}
      />
    );
  }

  render() {
    const { companyData, error } = this.props;
    return (
      <React.Fragment>
        {!companyData && !error && <Loading />}
        {error && <div className={style.error}>{error.toString()}</div>}
        {companyData && (
          <Container className={style.containerSpacing}>
            {this.header()}
            <People people={companyData.employees} />
          </Container>
        )}
      </React.Fragment>
    );
  }
}

export default OurCompany;
