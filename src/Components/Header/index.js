import React from 'react';
import cx from 'classnames';
import style from './style.module.scss';

export default class Header extends React.PureComponent {
  render() {
    const { className, primary, secondaryLeft, secondaryRight } = this.props;
    return (
      <div className={cx(style.header, className)}>
        {primary && <h1 className={style.title}>{primary}</h1>}
        {(secondaryLeft || secondaryRight) && (
          <div className={style.secondary}>
            {secondaryLeft && <div>{secondaryLeft}</div>}
            {secondaryRight && <div className={style.right}>{secondaryRight}</div>}
          </div>
        )}
      </div>
    );
  }
}
