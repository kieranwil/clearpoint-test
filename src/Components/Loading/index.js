import React from 'react';
import cx from 'classnames';
import style from './style.module.scss';

export const Loading = ({ className }) => <div className={cx(className, style.loading)} />;

export default Loading;
