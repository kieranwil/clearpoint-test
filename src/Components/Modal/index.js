import React from 'react';
import { createPortal } from 'react-dom';
import cx from 'classnames';
import style from './style.module.scss';

export default class Modal extends React.Component {
  static defaultProps = {
    open: true,
    onClickOutside: () => {},
    showClose: true,
    onClickClose: () => {}
  };

  node = undefined;

  componentDidMount() {
    this.node = document.createElement('div');
    this.node.classList = this.props.className || '';
    document.body.appendChild(this.node);
  }

  onClickInner = event => event.stopPropagation();

  renderNodes() {
    const {
      onClickInner,
      props: {
        open,
        onClickOutside,
        showClose,
        onClickClose,
        containerClass,
        innerClass,
        closeClass,
        children
      }
    } = this;
    if (!open) {
      return null;
    }
    const cClass = cx(style.containerClass, containerClass);
    const iClass = cx(style.innerClass, innerClass);
    const icClass = cx(style.closeClass, closeClass);
    return (
      <div onClick={onClickOutside} className={cClass}>
        <div onClick={onClickInner} className={iClass}>
          {showClose && (
            <div onClick={onClickClose} className={icClass}>
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                <path d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" />
              </svg>
            </div>
          )}
          {children}
        </div>
      </div>
    );
  }

  render() {
    const nodes = this.renderNodes();
    return nodes ? createPortal(this.renderNodes(), this.node) : null;
  }
}
