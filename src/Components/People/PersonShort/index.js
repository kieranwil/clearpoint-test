import React from 'react';
import cx from 'classnames';
import style from './style.module.scss';

export default class PersonShort extends React.PureComponent {
  personClicked = () => {
    this.props.highlight(this.props);
  };

  render() {
    const { className, avatar, firstName, lastName, bio } = this.props;
    return (
      <div className={cx(style.person, className)} onClick={this.personClicked}>
        <div className={style.avatar}>
          <img src={avatar} alt={firstName} />
        </div>
        <div className={style.info}>
          <p className={style.name}>
            {firstName} {lastName}
          </p>
          <p className={style.bio}>{bio}</p>
        </div>
      </div>
    );
  }
}
