import React from 'react';
import cx from 'classnames';
import style from './Cards.module.scss';

const Cards = ({ children, className }) => (
  <div className={cx(style.cards, className)}>{children}</div>
);

export default Cards;
