import React from 'react';
import cx from 'classnames';
import style from './style.module.scss';

const Container = ({ children, className }) => (
  <div className={cx(style.container, className)}>{children}</div>
);

export default Container;
