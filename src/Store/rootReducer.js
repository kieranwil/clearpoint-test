import { combineReducers } from 'redux';
import { companyDataReducer } from './CompanyData';

export default combineReducers({
  companyData: companyDataReducer
});
